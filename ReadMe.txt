
Note:
Please delete all the files and folders under esbsandbox except the following before creating archetype
- flows
- src
- pom.xml
- ReadMe.txt
Step 1: Run the following command from the current directory to create archetype based of sandbox project
mvn archetype:create-from-project

Step 2:Edit the pom file under the following folder from root of the esbsandbox project \target\generated-sources\archetype
Update the version number
<version>1.0.0</version>

Step 3: Run the following command
mvn clean install


How to create a project from archetype? 
Run the following command from the desired location to create mule project based of the sandbox archetype. It will create a folder with name matching the -DartifactId
Step 1: Update the -DarchetypeVersion=1.0.0-SNAPSHOT property in the step 2 below with the latest version number of archetype artifact.

Step 2:
mvn archetype:generate -DarchetypeGroupId=ca.rsagroup.sandbox -DarchetypeArtifactId=esbsigma-archetype -DarchetypeVersion=1.0.0-SNAPSHOT -DgroupId=ca.rsagroup.esbTest2 -DartifactId=esbTest2 -DinteractiveMode=false
