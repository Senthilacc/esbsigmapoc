<?xml version="1.0" encoding="UTF-8"?>
 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
     <xsl:output method="xml" indent="yes" />
	<xsl:param name="element" select="'DIARYH_PRIMARY_KEY'"/>	
	
	<xsl:template match="/">	
		<BROWSE>
	<xsl:choose>
		<xsl:when test="$element='DIARYH_PRIMARY_KEY'">
			<xsl:apply-templates select="BROWSE/DIARYH_PRIMARY_KEY"/>
		</xsl:when>
		<xsl:when test="$element='SGMCTL_ENTITY'">
			<xsl:apply-templates select="BROWSE/SGMCTL_ENTITY"/>
		</xsl:when>
	</xsl:choose>
			
	</BROWSE>	
</xsl:template>

<!-- 2) Filter out elements -->
	<xsl:template match="*">
		<xsl:copy-of select="." />
	</xsl:template>
</xsl:stylesheet>