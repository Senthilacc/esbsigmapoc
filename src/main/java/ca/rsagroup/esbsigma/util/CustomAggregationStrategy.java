package ca.rsagroup.esbsigma.util;

import org.mule.DefaultMuleEvent;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.routing.AggregationContext;
import org.mule.routing.AggregationStrategy;

public class CustomAggregationStrategy implements AggregationStrategy {

	@Override
	public MuleEvent aggregate(AggregationContext context) throws MuleException {
		 MuleEvent e = DefaultMuleEvent.copy(context.getEvents().get(0));
		    e.getMessage().setPayload(e.getMessage().getPayload());
		    return e;
	}

}
