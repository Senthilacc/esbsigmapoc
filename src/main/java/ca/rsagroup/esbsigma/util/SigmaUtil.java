package ca.rsagroup.esbsigma.util;

import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Date;
import java.util.Random;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import ca.rsagroup.sigma.dao.ISigmaQueueResponseDao;
import ca.rsagroup.sigma.domain.Sigmaqueueresponse;

public class SigmaUtil {

	private ISigmaQueueResponseDao sigmaQueueResponseDao;

	private String env;

	public void setSigmaQueueResponseDao(
			ISigmaQueueResponseDao sigmaQueueResponseDao) {
		this.sigmaQueueResponseDao = sigmaQueueResponseDao;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public void saveSigmaQueueResponseToDataBase(String id,
			String queueResponseString) {
		Sigmaqueueresponse sigmaqueueresponse = new Sigmaqueueresponse();
		sigmaqueueresponse.setId(id);
		sigmaqueueresponse.setResponseObject(queueResponseString);
		sigmaqueueresponse.setRecievedTimestamp(new Date());
		sigmaQueueResponseDao.insertSigmaQueueResponse(sigmaqueueresponse);
	}

	public boolean checkIfResponseIsAvailableToConsume(String messageToQueueID) {
		boolean foundResponse = false;
		try {
			if (sigmaQueueResponseDao.findSigmaQueueResponseByID(messageToQueueID) != null) {
				foundResponse = true;
			}
		} catch (Exception e) {	
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return foundResponse;
	}

	public String getSigmaQueueResponseByIDAndRemoveFromPersistence(
			String messageToQueueID) {
		Sigmaqueueresponse sigmaQueueResponse = sigmaQueueResponseDao
				.findSigmaQueueResponseByID(messageToQueueID);
		String sigmaResponse = null;
		if (sigmaQueueResponse != null) {
			sigmaResponse = sigmaQueueResponse.getResponseObject();
		}
		// eselectQueueResponseDao.deleteEselectQueueResponseByID(eselectQueueResponse);
		return sigmaResponse;
	}
	
	public static String addCorrelationIDToPayload(String payload, String correlationID){
		String correlaionIDPaddedTo64Characters = String.format("%1$-" + 64 + "s", correlationID);
		int firstPos = payload.indexOf("0001") + "0001".length();
		int lastPos =payload.indexOf("REQINFO");
		String returnPayloadString = payload.substring(0,firstPos) + correlaionIDPaddedTo64Characters + payload.substring(lastPos);
		return returnPayloadString;
	}

	public static String getCorrelationId(String policyNumber) {

		int correlationIDLength = 24;
		StringBuilder correlationID = new StringBuilder("");
		correlationID.append((new Date()).getTime());
		correlationID.append(policyNumber);
		if (correlationID.toString().length() >= correlationIDLength) {
			correlationID = correlationID.delete(correlationIDLength - 2,
					correlationID.length() + 1);
		}
		int numberOfRandomNumbersToAppend = correlationIDLength
				- correlationID.toString().length();
		if (correlationID.toString().length() < correlationIDLength) {
			for (int i = 0; i < numberOfRandomNumbersToAppend; i++) {
				correlationID.append(randInt(0, 9));
			}
		}
		return correlationID.toString();
	}

	public static String convertHexDecimalToString(String hexDecimalString) {
		byte[] bytes = null;
		try {
			bytes = Hex.decodeHex(hexDecimalString.toCharArray());
		} catch (DecoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String returnValue = null;
		try {
			returnValue = new String(bytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static boolean checkIfReponseFromQueueHasErrors(String payloadString) {
		boolean hasErrors = true;
		if (StringUtils.isNotBlank(payloadString)) {
			if (!payloadString.contains("ERROR")) {
				hasErrors = false;
			}
		}
		return hasErrors;
	}

	public static String getPolicyNumberFromResponseThatHasError(
			String payloadString) {
		String policyNumber = "";
		if (StringUtils.isNotBlank(payloadString)) {
			//String payloadStrippedWithMultipleWhiteSpaces = payloadString.trim().replaceAll(" +", "");
			String policyExtracted = payloadString.substring(
					payloadString.indexOf("ERROR")).substring(13).trim();
			policyNumber = policyExtracted.substring(0, policyExtracted.indexOf(" "));
		}
		return policyNumber;
	}
	
	public static String getPolicyNumberFromResponse(String payloadString){
		String correlationID = "";
		int startIndex = payloadString.indexOf("0001") + 4;
		int endIndex = 0;
		if(payloadString.contains("ERROR")){
			endIndex = payloadString.indexOf("ERROR");
			correlationID = payloadString.substring(startIndex,endIndex);
			 
		}
		else{
			endIndex = payloadString.indexOf("POLICY");
			correlationID = payloadString.substring(startIndex,endIndex);
		}
		return correlationID.trim();
	}
	
	public static String prefixZeorsOrTrimPolicyNumberToSevenCharacters(
			String poliyNumber) {
		StringBuilder updatedString = new StringBuilder("");
		if(StringUtils.isNotBlank(poliyNumber)){
			
			if(poliyNumber.length()<7){
				int numberOfZerosToPrefix = 7- poliyNumber.length();
				for (int i = 0; i < numberOfZerosToPrefix; i++) {
					updatedString.append("0");
				}
				updatedString.append(poliyNumber);
			}
			else{
				updatedString.append(poliyNumber.substring(0,7));
			}
		}
		return updatedString.toString();
	}

	public static String getPayLoadAsString(String payloadString) {
		return payloadString;
	}

	public boolean checkIfEnvIsProd() {
		boolean isProd = false;
		if(StringUtils.isNotBlank(env) && env.equals("PROD")){
			isProd = true;
		}
		return isProd;
	}

	public static String getPolicyNumber(String xmlString) {
		String pol = "";
		if(xmlString.indexOf("<policy number=\"")>0) {
			xmlString = xmlString.substring(xmlString.indexOf("<policy number=\"")+16);
			pol = xmlString.substring(0,xmlString.indexOf("\""));
		}
		else{
			pol = "" + (new Date()).getTime();
		}
		return pol;
		
	}
	
	public static String removeAccents(String text) {
		if(text!=null) text=text.replaceAll("é","e");
		return text == null ? null : Normalizer.normalize(text, Form.NFD)
				.replaceAll("\\p{InCombiningDiacriticalMarks}+", "").replaceAll("\\P{InBasic_Latin}+" , "");
	}
	
}
