package ca.rsagroup.exception;

import java.util.Map;

import org.mule.api.ExceptionPayload;
import org.mule.api.MuleException;
import org.mule.config.ExceptionHelper;

public class RSAExceptionPayload implements ExceptionPayload {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1933410686369157208L;

	private int code = 0;
	private String message = null;
	@SuppressWarnings("rawtypes")
	private Map info = null;
	private Throwable exception;

	public RSAExceptionPayload(String code, String message, Throwable exception) {
		this.code = Integer.parseInt(code);
		this.message = message;
		this.exception = exception;
		MuleException muleRoot = ExceptionHelper
				.getRootMuleException(exception);
		if (muleRoot != null) {
			this.message = muleRoot.getMessage();
			this.info = muleRoot.getInfo();
		} else if(message != null && message.isEmpty()) {
			this.message = exception.getMessage();
		}
	}

	@Override
	public Throwable getRootException() {
		return ExceptionHelper.getRootException(exception);
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getInfo() {
		return info;
	}

	@Override
	public Throwable getException() {
		return exception;
	}

}
