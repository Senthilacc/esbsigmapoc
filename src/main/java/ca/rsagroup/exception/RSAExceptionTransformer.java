package ca.rsagroup.exception;

import org.acord.standards.pc_surety.acord1.xml.AccountInqRs;
import org.acord.standards.pc_surety.acord1.xml.CInfinite;
import org.acord.standards.pc_surety.acord1.xml.ExtendedStatus;
import org.acord.standards.pc_surety.acord1.xml.ExtendedStatusCd;
import org.acord.standards.pc_surety.acord1.xml.MsgStatus;
import org.acord.standards.pc_surety.acord1.xml.MsgStatusCd;
import org.acord.standards.pc_surety.acord1.xml.ObjectFactory;
import org.acord.standards.pc_surety.acord1.xml.PolicyInqRs;
import org.apache.log4j.Logger;
import org.mule.api.ExceptionPayload;
import org.mule.api.MuleMessage;
import org.mule.api.security.UnauthorisedException;
import org.mule.api.transformer.TransformerMessagingException;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.util.ExceptionUtils;
import org.springframework.security.authentication.BadCredentialsException;

import ca.rsagroup.domain.ESBGenericErrorCode;
import ca.rsagroup.domain.Error;
import ca.rsagroup.schema.custom.xml.DiaryInqRs;

public class RSAExceptionTransformer extends AbstractMessageTransformer {

	private Logger log = Logger.getLogger(RSAExceptionTransformer.class
			.getName());

	/**
	 * Default constructor
	 */
	public RSAExceptionTransformer() {
	}
  
	@Override
	public Object transformMessage(MuleMessage message, String encoding) {
		// No exception, return incoming payload
		if (message.getExceptionPayload() == null) {
			return message.getPayload();
		}

		// Log exception
		Throwable exception = message.getExceptionPayload().getException();
		log.error("Caught exception: " + logException(exception));
		String responseType = (String)message.getInvocationProperty("ResponseType");

		// Process exception
		RSAExceptionPayload exceptionPayload = transformException(message);
		
		return buildMsgStatus(exceptionPayload, responseType);
	}

	protected RSAExceptionPayload transformException(MuleMessage msg) {

		final ExceptionPayload exceptionPayload = msg.getExceptionPayload();

		Throwable cause = exceptionPayload.getException().getCause();

		// TODO: Is this kosher to go back up to the exception if there's no
		// cause?
		if (cause == null) {
			cause = exceptionPayload.getException();
		}
		
		if (msg.getPayload() instanceof Error) {
			Error error = (Error) msg.getPayload(); 
			return new RSAExceptionPayload(
					ESBGenericErrorCode.InternalSystem.getCode(), 
					error.getErrorMessage(), cause);
		}
		else if (cause instanceof UnauthorisedException
				|| cause instanceof BadCredentialsException) {

			return new RSAExceptionPayload(
					ESBGenericErrorCode.Unauthorized.getCode(), cause
							.getMessage(), cause);
		}
		// I don't love this literal string value at all.
		else if (cause instanceof TransformerMessagingException
				&& cause.getMessage().contains(
						"Failed to transform from \"json")) {
			return new RSAExceptionPayload(
					ESBGenericErrorCode.Malformed.getCode(),
					cause.getMessage(), cause);
		} else {
			return new RSAExceptionPayload(
					ESBGenericErrorCode.InternalSystem.getCode(), cause
							.getMessage(), cause);
		}

	}

	/**
	 * @param e
	 * @return
	 */
	private String logException(Throwable e) {

		String newline = System.getProperty("line.separator");
		Throwable rootCause = rootCause(e);

		StringBuilder exceptionDetails = new StringBuilder("Root Cause: ")
				.append(rootCause.getClass().getName()).append(": ")
				.append(rootCause.getMessage());
		exceptionDetails.append(newline).append("Stack Trace:").append(newline);
		exceptionDetails.append(ExceptionUtils.getFullStackTrace(e));

		return exceptionDetails.toString();

	}

	private Throwable rootCause(Throwable e) {

		Throwable rootCause = ExceptionUtils.getRootCause(e);
		return rootCause == null ? e : rootCause;

	}
	
	private Object buildMsgStatus(RSAExceptionPayload payload, String responseType){
		ObjectFactory of = new ObjectFactory();
		MsgStatus msgStatus = of.createMsgStatus();
		MsgStatusCd msgStatusCode = of.createMsgStatusCd();
		msgStatusCode.setValue("ERROR");
		msgStatus.setMsgStatusCd(msgStatusCode);
		
		ExtendedStatusCd statusCd = of.createExtendedStatusCd();
		statusCd.setValue(String.valueOf(payload.getCode()));
		ExtendedStatus extendedStatus = of.createExtendedStatus();
		extendedStatus.setExtendedStatusCd(statusCd);
		CInfinite infinite = of.createCInfinite();
		infinite.setValue(payload.getMessage()==null?payload.getException().getMessage():payload.getMessage());
		extendedStatus.setExtendedStatusDesc( infinite );
		msgStatus.getExtendedStatuses().add( extendedStatus );
		
		return wrapMsgStatus(msgStatus,responseType);
		
	}
	
	private Object wrapMsgStatus(MsgStatus msgStatus, String responseType) {
		if("PolicyInqRs".equalsIgnoreCase(responseType)){
			return wrapWithPolicyInqResponse( msgStatus );
		}else if("AccountInqRs".equals(responseType)){
			return wrapWithAccountInqResponse(msgStatus);
		}else if("DiaryInqRs".equalsIgnoreCase(responseType)){
			return wrapWithDiaryInqResponse(msgStatus);
		}
		return null;
		
	}

	private PolicyInqRs wrapWithPolicyInqResponse(MsgStatus msgStatus){
		ObjectFactory obj = new ObjectFactory();
		PolicyInqRs res = obj.createPolicyInqRs();
		res.setMsgStatus( msgStatus);
		return res;
	}
	
	private AccountInqRs wrapWithAccountInqResponse(MsgStatus msgStatus){
		ObjectFactory obj = new ObjectFactory();
		AccountInqRs res = obj.createAccountInqRs();
		res.setMsgStatus( msgStatus);
		return res;
	}
	
	private DiaryInqRs wrapWithDiaryInqResponse(MsgStatus msgStatus){
		ca.rsagroup.schema.custom.xml.ObjectFactory obj = new ca.rsagroup.schema.custom.xml.ObjectFactory();
		DiaryInqRs res = obj.createDiaryInqRs();
		res.setMsgStatus( msgStatus);
		return res;
	}

}
