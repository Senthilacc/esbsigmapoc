package ca.rsagroup.transformers;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

import ca.rsagroup.utilties.XmlMerger;

public class CollectionToXML extends AbstractMessageTransformer {

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding)
			throws TransformerException {
		List<String> payloadMessages = new CopyOnWriteArrayList<String>();
    	payloadMessages = (List<String>) message.getPayload();
    	
		XmlMerger merger = new XmlMerger("ns2:PolicyDetailsResponse", "ns2", "http://www.rsagroup.ca/schema/custom/xml/");

		for (String payloadMessage : payloadMessages) {
    		merger.appendXml(payloadMessage);
    	}
    	
    	return merger.toString();
	}

}
