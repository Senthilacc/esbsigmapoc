package ca.rsagroup.suspend.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class SuspendPolicyTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	   
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,suspendedpolicysearch.xml";
	}
	
	@Test
	public void testSuspendPolicyHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("suspended/SEARCH_RESPONSE.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("suspended/PolicyInqRq.xml");
		
		MuleEvent event = runFlow("SuspendedPolicySearch", testEvent(webserviceResponse));
		
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//SEARCH/SGMCTL_ENTITY/USRPF", searchRq);
		
		String policyInqRs = removeNamespace(event.getMessage());
        XMLAssert.assertXpathEvaluatesTo("OK","//MsgStatus/MsgStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("SAMER","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/Surname", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("ABBOUD","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/GivenName", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("8233607","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyNumber", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("AP","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/LOBCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/EffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20160101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/ExpirationDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("NB","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/PolicyStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150114","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/TransactionEffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("9","//PolicyInqRs/MaxResults", policyInqRs);
	} 
	
	@Test
	public void testSuspendPolicyNoResult() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("suspended/SEARCH_RESPONSE_NO_RESULT.xml")));
		String webserviceResponse = readFile("suspended/PolicyInqRq.xml");

		MuleEvent event = runFlow("SuspendedPolicySearch",
				testEvent(webserviceResponse));

		String policyInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK", "//MsgStatus/MsgStatusCd",
				policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("0","//PolicyInqRs/MaxResults", policyInqRs);
	}
	
	@Test
	public void testSuspendPolicyEdrMsg() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("suspended/SEARCH_RESPONSE_EDR_MSG.xml")));
		String webserviceResponse = readFile("suspended/PolicyInqRq.xml");

		MuleEvent event = runFlow("SuspendedPolicySearch",
				testEvent(webserviceResponse));
		String policyInqRs = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("1256", "//MsgStatus/ExtendedStatus[1]/ExtendedStatusCd", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("OK", "//MsgStatus/MsgStatusCd", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//PolicyInqRs/MaxResults", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("SAMER","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/Surname", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("ABBOUD","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/GivenName", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("8233607","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyNumber", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("AP","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/LOBCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/EffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20160101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/ExpirationDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("NB","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/PolicyStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150114","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/TransactionEffectiveDt", policyInqRs);
	}
	
}
