package ca.rsagroup.policy.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class PolicySearchTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	//private static final String BASE_DIRECTORY ="WSDL/sigma/suspended/";
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,policysearch.xml";
	}
	
	@Test
	public void testPolicySearchWithPolicyNoHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("policy/SOAPPolicySearchWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String websvcResponse = readFile("policy/PolicySearch_CSIO_Request_PolicyInqRq.xml");
		
		MuleEvent event = runFlow("PolicySearchFlow", testEvent(websvcResponse));
		
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//SEARCH/SGMCTL_ENTITY[1]/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("8199435","//SEARCH/POLICY_SEARCH_KEYS[1]/POLNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("C","//SEARCH/POLICY_SEARCH_KEYS[1]/KFNTX", searchRq);
		
       	String policyInqRs = removeNamespace(event.getMessage());
        XMLAssert.assertXpathEvaluatesTo("OK","//MsgStatus/MsgStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("BENJAMIN","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/Surname", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo(" B.FIGUEROA","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/GivenName", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("100 PLACE RIVARD","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/Addr1", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("LAVAL","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/City", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("                       QC","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/StateProv", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("H7E 4W6","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/PostalCode", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1663319","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/ItemIdInfo/InsurerId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("19710325","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo/PersonInfo/BirthDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("8199435","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyNumber", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("D","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("AP","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/LOBCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20110320","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/EffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20120320","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/ExpirationDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1143","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ItemIdInfo/AgencyId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1663319","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ItemIdInfo/InsurerId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1","//PolicyInqRs/MaxResults", policyInqRs);
	} 
	
	@Test
	public void testPolicySearchNoResult() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("policy/SOAPPolicySearchWSResponseNoResults.xml")));
		String webserviceResponse = readFile("policy/PolicySearch_CSIO_Request_NoResults_PolicyInqRq.xml");

		MuleEvent event = runFlow("PolicySearchFlow",
				testEvent(webserviceResponse));

		String policyInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK", "//MsgStatus/MsgStatusCd",
				policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("","//PolicyInqRs/MaxResults", policyInqRs);
	}
	
	/*@Test
	public void testPolicySearchEdrMsg() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("SEARCH_RESPONSE_EDR_MSG.xml")));
		String webserviceResponse = readFile("ClientBrowse_CSIO_Request_PolicyInqRq.xml");

		MuleEvent event = runFlow("PolicySearchFlow",
				testEvent(webserviceResponse));
		String policyInqRs = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("1256", "//MsgStatus/ExtendedStatus[1]/ExtendedStatusCd", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("OK", "//MsgStatus/MsgStatusCd", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//PolicyInqRs/MaxResults", policyInqRs);
		XMLAssert.assertXpathEvaluatesTo("SAMER","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/Surname", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo(" ABBOUD","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/GivenName", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("8233607","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyNumber", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("AP","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/LOBCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/EffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20160101","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/ExpirationDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("NB","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/PolicyStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20150114","//PolicyInqRs/TransactionInqInfo/TransactionInfo[1]/TransactionEffectiveDt", policyInqRs);
	}
	
	@Test
	public void testPolicySearchWithNameHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("SOAPPolicySearchWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String websvcResponse = readFile("ClientBrowse_CSIO_Request_PolicyInqRq.xml");
		
		MuleEvent event = runFlow("PolicySearchFlow", testEvent(websvcResponse));
		
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//SEARCH/SGMCTL_ENTITY[1]/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("8199435","//SEARCH/POLICY_SEARCH_KEYS[1]/POLNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("C","//SEARCH/POLICY_SEARCH_KEYS[1]/KFNTX", searchRq);
		
       	String policyInqRs = removeNamespace(event.getMessage());
        XMLAssert.assertXpathEvaluatesTo("OK","//MsgStatus/MsgStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("BENJAMIN","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/Surname", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo(" B.FIGUEROA","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/NameInfo[1]/PersonName/GivenName", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("100 PLACE RIVARD","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/Addr1", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("LAVAL","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/City", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("                       QC","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/StateProv", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("H7E 4W6","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/Addr[1]/PostalCode", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1663319","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/GeneralPartyInfo/ItemIdInfo/InsurerId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("19710325","//PolicyInqRs/PartyInqInfo/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo/PersonInfo/BirthDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("8199435","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyNumber", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("D","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/PolicyStatusCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("AP","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/LOBCd", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20110320","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/EffectiveDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("20120320","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ContractTerm/ExpirationDt", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1143","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ItemIdInfo/AgencyId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1663319","//PolicyInqRs/PartyInqInfo/PartialPolicy[1]/ItemIdInfo/InsurerId", policyInqRs);
        XMLAssert.assertXpathEvaluatesTo("1","//PolicyInqRs/MaxResults", policyInqRs);
	} 
	*/
	
}
