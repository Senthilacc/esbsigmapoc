package ca.rsagroup.bcalerts;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class BCAlertsPostalcodeBrowseTest extends BaseFunctionalMunitSuite { 
	 
	private String bcPostalCdBrowseRq;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commonbcalerts.xml,bcalertspostalcodebrowse.xml";
	}
	
	@Test
	public void testBCAlertsPostalCdBrowseHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsPostalcodeBrowse_WS_Response.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(bcPostalCdBrowseRq == null ) {
					bcPostalCdBrowseRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(bcPostalCdBrowseRq, event.getMuleContext());
					bcPostalCdBrowseRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsPostalCodeBrowse_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCPostalcodeBrowse", testEvent(webserviceResponse));
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("600","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KNOCV", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("G1G 2S7","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KCODP", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("BOURDAIS","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KRUE", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//BROWSE_UNRPHY/SGMCTL_ENTITY[1]/USRPF", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("01","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KCIE", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("5","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KAPPA", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("P","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KCODE", bcPostalCdBrowseRq);

		
		//Assert Response
		String responsexml = removeNamespace(event.getMessage());

		XMLAssert.assertXpathEvaluatesTo("5","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/UnitNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("600","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("ACCDI","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/KCatCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/StateProv", responsexml);
		XMLAssert.assertXpathEvaluatesTo("G1G 2S7","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/PostalCode", responsexml);
		XMLAssert.assertXpathEvaluatesTo("01","//AccountInqRs/PartyInqInfo[1]/PartialPolicy[1]/ItemIdInfo[1]/AgencyId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("TEST","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/SourceCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20141021","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/ApplicationWrittenDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("BOURDAIS","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("P","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/NeighbourhoodConditionCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/ContractNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QUEBEC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/City", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
	  
	} 
	
	
	@Test
	public void testBCAlertsPostalBrowseWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsPostalcodeBrowse_WS_Response_noResults.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(bcPostalCdBrowseRq == null ) {
					bcPostalCdBrowseRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(bcPostalCdBrowseRq, event.getMuleContext());
					bcPostalCdBrowseRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsPostalCodeBrowse_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCPostalcodeBrowse", testEvent(webserviceResponse));
		
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//BROWSE_UNRPHY/SGMCTL_ENTITY[1]/USRPF", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("600","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KNOCV", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("01","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KCIE", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("G1G 2S7","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KCODP", bcPostalCdBrowseRq);
		XMLAssert.assertXpathEvaluatesTo("BOURDAIS","//BROWSE_UNRPHY/UNRPHY_PRIMARY_KEY[1]/KRUE", bcPostalCdBrowseRq);
		


		//Assert Response
		String accountInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus/MsgStatusCd", accountInqRs);
		XMLAssert.assertXpathNotExists("//AccountInqRs/PartyInqInfo[1]", accountInqRs);
	} 
	 
	
}
