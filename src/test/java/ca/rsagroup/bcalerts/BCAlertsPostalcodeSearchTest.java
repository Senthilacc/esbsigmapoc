package ca.rsagroup.bcalerts;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class BCAlertsPostalcodeSearchTest extends BaseFunctionalMunitSuite { 
	 
	private String bcPostalCdSrchRq;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commonbcalerts.xml,bcalertspostalcodesearch.xml";
	}
	
	@Test
	public void testBCAlertsPostalCdSearchHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsPostalcodeSearch_WS_Response.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(bcPostalCdSrchRq == null ) {
					bcPostalCdSrchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(bcPostalCdSrchRq, event.getMuleContext());
					bcPostalCdSrchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsPostalCodeSearch_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCPostalcodeSearch", testEvent(webserviceResponse));
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//SEARCH_UNRPHY[1]/SGMCTL_ENTITY[1]/USRPF", bcPostalCdSrchRq);
		XMLAssert.assertXpathEvaluatesTo("01","//SEARCH_UNRPHY[1]/UNRPHY_NATURAL_KEYS[1]/KCIE", bcPostalCdSrchRq);
		XMLAssert.assertXpathEvaluatesTo("J2C 4R4","//SEARCH_UNRPHY[1]/UNRPHY_NATURAL_KEYS[1]/KCODP", bcPostalCdSrchRq);
		
		//Assert Response
		String responsexml = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("MELANCON","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("328","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/StateProv", responsexml);
		XMLAssert.assertXpathEvaluatesTo("J2C 4R4","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/PostalCode", responsexml);
		XMLAssert.assertXpathEvaluatesTo("01","//AccountInqRs/PartyInqInfo[1]/PartialPolicy[1]/ItemIdInfo[1]/AgencyId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("DRUMMONDVILLE","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/City", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
	  
	} 
	
	
	@Test
	public void testBCAlertsNameSearchWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsPostalcodeSearch_WS_Response_noResults.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(bcPostalCdSrchRq == null ) {
					bcPostalCdSrchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(bcPostalCdSrchRq, event.getMuleContext());
					bcPostalCdSrchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsPostalCodeSearch_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCPostalcodeSearch", testEvent(webserviceResponse));
		
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//SEARCH_UNRPHY[1]/SGMCTL_ENTITY[1]/USRPF", bcPostalCdSrchRq);
		XMLAssert.assertXpathEvaluatesTo("J2C 4R4","//SEARCH_UNRPHY[1]/UNRPHY_NATURAL_KEYS[1]/KCODP", bcPostalCdSrchRq);
		
		//Assert Response
		String accountInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus/MsgStatusCd", accountInqRs);
		XMLAssert.assertXpathNotExists("//AccountInqRs/PartyInqInfo[1]", accountInqRs);
	} 
	 
	
}
