package ca.rsagroup.bcalerts;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class TestError extends BaseFunctionalMunitSuite{
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml";
	}
	
	@Test 
	public void test() throws MuleException, Exception{
		
		MuleEvent event = runFlow("TestMsgStats", testEvent(""));
		
		System.out.println(event.getMessage().getPayload()!=null);
		
	}

}
