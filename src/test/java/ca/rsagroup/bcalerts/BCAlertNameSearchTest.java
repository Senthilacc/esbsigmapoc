package ca.rsagroup.bcalerts;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class BCAlertNameSearchTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commonbcalerts.xml,bcalertsnamesearch.xml";
	}
	
	@Test
	public void testBCAlertsNameSearchHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsNameSearch_WS_Response.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsNameSearch_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCNameSearch", testEvent(webserviceResponse));
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//SEARCH_UNRMPL[1]/SGMCTL_ENTITY[1]/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("BRETONVICKY","//SEARCH_UNRMPL[1]/UNRMPL_NATURAL_KEYS[1]/KNOMR", searchRq);
		
		
		//Assert Response
		String responsexml = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("19500101","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/BirthDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("10","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/UnitNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("FRONTENAC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("328","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/StateProv", responsexml);
		XMLAssert.assertXpathEvaluatesTo(" BRETON","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/GivenName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("J2B 1A1","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/PostalCode", responsexml);
		XMLAssert.assertXpathEvaluatesTo("01","//AccountInqRs/PartyInqInfo[1]/PartialPolicy[1]/ItemIdInfo[1]/AgencyId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("DRUMMONDVILLE","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/City", responsexml);
		XMLAssert.assertXpathEvaluatesTo("VICKY","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/Surname", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
		  
	} 
	
	
	@Test
	public void testBCAlertsNameSearchWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsNameSearch_WS_Response_noResults.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsNameSearch_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCNameSearch", testEvent(webserviceResponse));
		
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//SEARCH_UNRMPL[1]/SGMCTL_ENTITY[1]/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("BRETONVICKY","//SEARCH_UNRMPL[1]/UNRMPL_NATURAL_KEYS[1]/KNOMR", searchRq);
		
		//Assert Response
		String accountInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus/MsgStatusCd", accountInqRs);
		XMLAssert.assertXpathNotExists("//AccountInqRs/PartyInqInfo[1]", accountInqRs);
	} 
	 
	
}
