package ca.rsagroup.bcalerts;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class BCAlertNameBrowseTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commonbcalerts.xml,bcalertsnamebrowse.xml";
	}
	
	@Test
	public void testBCAlertsNameBrowseHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsNameBrowse_WS_Response.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsNameBrowse_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCNameBrowse", testEvent(webserviceResponse));
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("BRETONVICKY","//BROWSE_UNRMPL/UNRMPL_PRIMARY_KEY[1]/KNOMR", searchRq);
		XMLAssert.assertXpathEvaluatesTo("01","//BROWSE_UNRMPL/UNRMPL_PRIMARY_KEY[1]/KCIE", searchRq);
		XMLAssert.assertXpathEvaluatesTo("0.00","//BROWSE_UNRMPL/UNRMPL_PRIMARY_KEY[1]/KSEQU", searchRq);
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//BROWSE_UNRMPL/SGMCTL_ENTITY[1]/USRPF", searchRq);

		
		//Assert Response
		String responsexml = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("10","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/UnitNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("B342006097986","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/License[1]/LicensePermitNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("19500101","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/BirthDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("328","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("TEST","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/IncidentDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/StateProv", responsexml);
		XMLAssert.assertXpathEvaluatesTo("J2B 1A1","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/PostalCode", responsexml);
		XMLAssert.assertXpathEvaluatesTo("01","//AccountInqRs/PartyInqInfo[1]/PartialPolicy[1]/ItemIdInfo[1]/AgencyId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("[JOUR]","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/SourceCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20141022","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/ApplicationWrittenDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("19990101","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/EventDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("FRONTENAC","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetName", responsexml);
		XMLAssert.assertXpathEvaluatesTo(" BRETON","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/GivenName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("[CRIM]","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/RiskCat[1]/NeighbourhoodConditionCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/RiskAlert[1]/ContractNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("DRUMMONDVILLE","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/City", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("VICKY","//AccountInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/Surname", responsexml);

		  
	} 
	
	
	@Test
	public void testBCAlertsNameBrowseWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("bcalerts/BCAlertsNameBrowse_WS_Response_noResults.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("bcalerts/BCAlertsNameBrowse_CSIO_Request_AccountInqRq.xml");
		
		MuleEvent event = runFlow("BCNameBrowse", testEvent(webserviceResponse));
		
		
		//Assert Webservice request
		XMLAssert.assertXpathEvaluatesTo("BRETONVICKY","//BROWSE_UNRMPL/UNRMPL_PRIMARY_KEY[1]/KNOMR", searchRq);
		XMLAssert.assertXpathEvaluatesTo("UCQI0468","//BROWSE_UNRMPL/SGMCTL_ENTITY[1]/USRPF", searchRq);
		
		//Assert Response
		String accountInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//AccountInqRs/MsgStatus/MsgStatusCd", accountInqRs);
		XMLAssert.assertXpathNotExists("//AccountInqRs/PartyInqInfo[1]", accountInqRs);
	} 
	 
	
}
