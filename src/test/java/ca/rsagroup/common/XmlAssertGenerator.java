package ca.rsagroup.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.DocumentResult;
import org.dom4j.io.DocumentSource;
import org.dom4j.io.SAXReader;

public class XmlAssertGenerator {

	public static void main(String[] args) throws DocumentException, TransformerException {
		new XmlAssertGenerator().generate();
	}

	public void generate() throws DocumentException, TransformerException {
		File file = new File("D:/test.xml");
		SAXReader reader = new SAXReader(); 
		Document document = reader.read(file);
		document = removeNamespace(document);
		Element root = document.getRootElement();
		List<Dom> contentElements = getChildrensWithContent(
				new ArrayList<Dom>(), root);
		Set<String> xpaths = new HashSet<String>();
		for (Dom contentElement : contentElements) {
			xpaths.add(generateXpath(contentElement));
		}

		for (String xpath : xpaths) {
			if(document.selectSingleNode(xpath) != null) {
			System.out.println("XMLAssert.assertXpathEvaluatesTo(\""
					+ document.selectSingleNode(xpath).getText() + "\",\""
					+ xpath + "\", responsexml);");
			}
		}
		
	}
	 

	@SuppressWarnings("unchecked")
	public List<Dom> getChildrensWithContent(List<Dom> contentElements,
			Element element) {
		for (Iterator<Element> i = element.elementIterator(); i.hasNext();) {
			Element child = i.next();
			if (!child.getTextTrim().isEmpty()) {
				Dom dom = new Dom(child, "");
				dom.setOriginalElement(element);
				contentElements.add(dom);
			}
			getChildrensWithContent(contentElements, child);
		}
		return contentElements;
	}

	public String generateXpath(Dom dom) {
		if (dom.getXpath().isEmpty()) {
			dom.setXpath(getElementName(dom.getCurrentElement()));
		} else {

			if (!dom.getCurrentElement().isRootElement()) {
				dom.setXpath(getElementName(dom.getCurrentElement()) + "[1]/"
						+ dom.getXpath());
			} else {
				dom.setXpath(getElementName(dom.getCurrentElement()) + "/"
						+ dom.getXpath());
			}
		}

		Element parent = dom.getCurrentElement().getParent();
		if (parent != null) {
			dom.setCurrentElement(parent);
			generateXpath(dom);
		} else {
			dom.setXpath("//" + dom.getXpath());
		}
		return dom.getXpath();
	}

	private String getElementName(Element element) {
		return (element.getNamespacePrefix().isEmpty()) ? element.getName()
				: element.getNamespacePrefix() + ":" + element.getName();
	}

	public class Dom {

		private String xpath;

		private Element currentElement;

		private Element originalElement;

		public Dom(Element currentElement, String xpath) {
			this.setCurrentElement(currentElement);
			this.setXpath(xpath);
		}

		public String getXpath() {
			return xpath;
		}

		public void setXpath(String xpath) {
			this.xpath = xpath;
		}

		public Element getCurrentElement() {
			return currentElement;
		}

		public void setCurrentElement(Element currentElement) {
			this.currentElement = currentElement;
		}

		public Element getOriginalElement() {
			return originalElement;
		}

		public void setOriginalElement(Element originalElement) {
			this.originalElement = originalElement;
		}
	}

	private Document removeNamespace(Document document) throws TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(new StreamSource(
				"src/main/resources/removeNamespaces.xsl"));
		DocumentSource source = new DocumentSource(document);
		DocumentResult result = new DocumentResult();
		transformer.transform(source, result);
		return result.getDocument();
	}

}
