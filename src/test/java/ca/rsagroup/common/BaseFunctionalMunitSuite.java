package ca.rsagroup.common;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.mule.api.MuleMessage;
import org.mule.api.config.MuleProperties;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.module.xml.transformer.XsltTransformer;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.transformer.types.SimpleDataType;

import ca.rsagroup.sigma.test.utils.ReadFile;

public class BaseFunctionalMunitSuite extends FunctionalMunitSuite {
	
	protected static final String BASE_DIRECTORY ="artifacts/";
	

	@Override
	protected Properties getStartUpProperties() {
		Properties properties = new Properties(super.getStartUpProperties());
		properties.put(MuleProperties.APP_HOME_DIRECTORY_PROPERTY, new File(
				"mappings").getAbsolutePath());
		return properties;
	} 
	

	protected String removeNamespace(MuleMessage message)
			throws InitialisationException, org.mule.api.transformer.TransformerException {
		XsltTransformer transformr = new XsltTransformer();
		transformr.setReturnDataType( new SimpleDataType<Object>(String.class) );
		transformr.setXslFile("removeNamespaces.xsl");
		transformr.initialise();
		Object obj = transformr.transformMessage(message,"UTF-8");
		return (String)obj;
	}
	
	protected String readFile(String fileName) throws IOException {
		return ReadFile.read(BASE_DIRECTORY + fileName);
	}
	
	protected static String readFileAsString(String fileName) throws IOException {
		return ReadFile.read(BASE_DIRECTORY + fileName);
	}
}
