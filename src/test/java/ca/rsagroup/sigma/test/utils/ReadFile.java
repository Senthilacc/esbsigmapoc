package ca.rsagroup.sigma.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader; 

import org.apache.commons.io.IOUtils;

public class ReadFile {

	//File file = new File(".\\src\\test\\java\\ca\\rsagroup\\client\\browse\\ClientBrowse_CSIO_PolicyInqRq.xml");
	public static String readFile(String filePath) throws IOException{
        StringBuilder sb = new StringBuilder();
        File file = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(file));
    	   String sCurrentLine =null;
			    while ((sCurrentLine = br.readLine()) != null) {
			        sb.append(sCurrentLine);
			    }

			    br.close();
      return sb.toString();
	}
	
	/**
	 * @param filePath
	 * @return file content
	 * @throws IOException
	 * 
	 * Reads the file from classpath
	 */
	public static String read(String filePath) throws IOException{
		InputStream is = ReadFile.class.getClassLoader().getResourceAsStream(filePath);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
    	   String sCurrentLine =null;
			    while ((sCurrentLine = br.readLine()) != null) {
			        sb.append(sCurrentLine);
			    }

			    br.close();
			    is.close();
      return sb.toString();
	} 
	
	public static String readFromTestResources(String filePath) throws IOException{ 
      return IOUtils.toString(ReadFile.class.getClassLoader().getResourceAsStream(filePath),"UTF-8");
	} 
	
}
