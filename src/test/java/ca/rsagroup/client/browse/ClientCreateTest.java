package ca.rsagroup.client.browse;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class ClientCreateTest extends BaseFunctionalMunitSuite {


	private String request;

	@Override
	protected String getConfigResources() {
		return "commonFlows.xml, client.xml";
	}

	@Test
	public void testClientCreateHappyPath() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("client/ClientCreateWSResponse.xml"))); 
		
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(request == null ) {
					request = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(request, event.getMuleContext());
					request = removeNamespace(newMessage);
				}
			}
		});


		String webserviceResponse = readFile("client/ClientCreate_CSIO_Request_PolicyInqRq.xml");
		
		MuleEvent event = runFlow("CreateClientFlow", testEvent(webserviceResponse)); 
		
		String responsexml = removeNamespace(event.getMessage()); 
		
		// Assert Webservice request
		
		XMLAssert.assertXpathEvaluatesTo("MONICA CHOUINARD","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/CLNAM", request);
		XMLAssert.assertXpathEvaluatesTo("52125","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KCLAG", request);
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//GENERIC_REQUEST/SGMCTL_ENTITY[1]/USRPF", request);
		XMLAssert.assertXpathEvaluatesTo("807923","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/CLTKY", request);
		XMLAssert.assertXpathEvaluatesTo("G1B 3K5","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/PSTCD", request);
		XMLAssert.assertXpathEvaluatesTo("F","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KAMST", request);
		XMLAssert.assertXpathEvaluatesTo("4521125","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/FAXNO", request);
		XMLAssert.assertXpathEvaluatesTo("N","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/MAILF", request);
		XMLAssert.assertXpathEvaluatesTo("INS","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KKSPI", request);
		XMLAssert.assertXpathEvaluatesTo("20140208","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/APPDT", request);
		XMLAssert.assertXpathEvaluatesTo("19850409","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KDDOB", request);
		XMLAssert.assertXpathEvaluatesTo("EC","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/ASICD", request);
		XMLAssert.assertXpathEvaluatesTo("5214","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KGREM", request);
		XMLAssert.assertXpathEvaluatesTo("2145872","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/PHONE", request);
		XMLAssert.assertXpathEvaluatesTo("F","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/SEXMF", request);
		XMLAssert.assertXpathEvaluatesTo("4512","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KPOST", request);
		XMLAssert.assertXpathEvaluatesTo("QUEBEC QC","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/ADDR3", request);
		XMLAssert.assertXpathEvaluatesTo("0","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/KTAXE", request);
		XMLAssert.assertXpathEvaluatesTo("N","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/STREQ", request);
		XMLAssert.assertXpathEvaluatesTo("54541245","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/BUSPH", request);
		XMLAssert.assertXpathEvaluatesTo("234 BOUL LAURIER","//GENERIC_REQUEST/CLTMAS_ENTITY[1]/ADDR1", request);


		
		//Assert Response
		
		XMLAssert.assertXpathEvaluatesTo("234","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QC","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/StateProv", responsexml);
		XMLAssert.assertXpathEvaluatesTo("5214","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/EmployerCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("INS","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/InsuredOrPrincipalRoleCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("CHOUINARD","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/GivenName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20140208","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/EffectiveDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("BOUL","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetName", responsexml);
		XMLAssert.assertXpathEvaluatesTo("LAURIER","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/DetailAddr[1]/StreetTypeCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("MONICA","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/NameInfo[1]/PersonName[1]/Surname", responsexml);
		XMLAssert.assertXpathEvaluatesTo("52125","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/ItemIdInfo[1]/AgencyId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("19850409","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/BirthDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("QUEBEC","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Addr[1]/City", responsexml);
		XMLAssert.assertXpathEvaluatesTo("F","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/MaritalStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Business","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Communications[1]/PhoneInfo[1]/PhoneTypeCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//PolicyInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("54541245","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/Communications[1]/PhoneInfo[1]/PhoneNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("F","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/GenderCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("807923","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/GeneralPartyInfo[1]/ItemIdInfo[1]/InsurerId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("EC","//PolicyInqRs/PartyInqInfo[1]/InsuredOrPrincipal[1]/InsuredOrPrincipalInfo[1]/PersonInfo[1]/OccupationClassCd", responsexml);


	}
	
	@Test
	public void testClientCreateEdrMsgEntity() throws MuleException, Exception {
		whenMessageProcessor("outbound-endpoint")
		.ofNamespace("http")
		.thenReturn(
				muleMessageWithPayload(readFile("client/EDRMSG_ENTITY_ClientCreateWSResponse.xml")));  
		
		String webserviceResponse = readFile("client/ClientCreate_CSIO_Request_PolicyInqRq.xml");
		
		MuleEvent event = runFlow("CreateClientFlow", testEvent(webserviceResponse)); 
		String responsexml = removeNamespace(event.getMessage()); 
		 	
		//Assert Response
		
		XMLAssert.assertXpathEvaluatesTo("OK","//PolicyInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("604","//PolicyInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Validation Failed","//PolicyInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusDesc", responsexml);

	}

}
