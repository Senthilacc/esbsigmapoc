package ca.rsagroup.client.browse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.custommonkey.xmlunit.XMLAssert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.api.MessagingException;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.module.xml.transformer.XmlToXMLStreamReader;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class ClientBrowseTest extends BaseFunctionalMunitSuite{ 
	 
	static String clientBrowseWSResponse = null;
	static String csioRequest = null;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,clientbrowse.xml";
	}
	
	@Override
	protected List<String> getFlowsExcludedOfInboundDisabling()
    {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("clientBrowseVmEndpoint");
		return arrayList;
    }
	@Override
	 protected boolean haveToMockMuleConnectors()
	    {
	        return false;
	    }
	
	@BeforeClass
	public static void readFile() throws IOException{
		clientBrowseWSResponse = readFileAsString("client/ClientBrowseWSResponse.xml");
		csioRequest = readFileAsString("client/ClientBrowse_CSIO_Request_PolicyInqRq.xml");
		
	}
	 
	@Test
	public void testClientBrowseResponseDataMapperFlow_happyPath() throws MuleException, Exception{
		
		
		MuleEvent event = runFlow("clientBrowseResponseDataMapperFlow",testEvent(clientBrowseWSResponse));
		
		String formattedXML = removeNamespace(event.getMessage());
		
        XMLAssert.assertXpathEvaluatesTo("OK","//MsgStatus/MsgStatusCd",formattedXML);
        
		XMLAssert.assertXpathEvaluatesTo("807923", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/ItemIdInfo/InsurerId", formattedXML);
		
		//Name Info Assert
		XMLAssert.assertXpathEvaluatesTo("SENTHIL", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("CHINRAJ", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName", formattedXML);
		
		//Address Information Assert
		XMLAssert.assertXpathEvaluatesTo("Leith Hill Road", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("24", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("Apt", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("1222", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("TORONTO", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/City", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("ON", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProv", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("M2J1Z3", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/PostalCode", formattedXML);
		//Phone details assert
		XMLAssert.assertXpathEvaluatesTo("Business", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[1]/PhoneTypeCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("5195007000", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[1]/PhoneNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("Fax", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[2]/PhoneTypeCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("5195006000", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[2]/PhoneNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("Business", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[3]/PhoneTypeCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("5000", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[3]/PhoneNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("Personal", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[4]/PhoneTypeCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("5195005666", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Communications/PhoneInfo[4]/PhoneNumber", formattedXML);
		//PersonalInformationAssert
		XMLAssert.assertXpathEvaluatesTo("M", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/InsuredOrPrincipalInfo/PersonInfo/GenderCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("19841111", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/InsuredOrPrincipalInfo/PersonInfo/BirthDt", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("S", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/InsuredOrPrincipalInfo/PersonInfo/MaritalStatusCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("O", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/InsuredOrPrincipalInfo/PersonInfo/OccupationClassCd", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("EGCode", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/InsuredOrPrincipalInfo/PersonInfo/EmployerCd", formattedXML);
	}
 
	@Test
	public void testClientBrowseRequestDataMapperFlow() throws MuleException, Exception{
		
		MuleEvent muleevent = runFlow("clientBrowseRequestDataMapperFlow", testEvent(csioRequest));
		String formattedXML = removeNamespace( muleevent.getMessage());
		XMLAssert.assertXpathEvaluatesTo("807923.00", "/BROWSE/CLTMAS_PRIMARY_KEY/CLTKY", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("UCQI0268", "/BROWSE/SGMCTL_ENTITY/USRPF", formattedXML);
	}
	
	@Test
	public void testClientBrowseResponseDataMapperFlow__nullCheck() throws MuleException, Exception{
		String webserviceResponse = clientBrowseWSResponse.replace("<ADDR3>TORONTO ON</ADDR3>", "<ADDR3></ADDR3>");
		webserviceResponse = webserviceResponse.replace("<ADDR1>24 Leith Hill Road</ADDR1>", "<ADDR1></ADDR1>");
		webserviceResponse = webserviceResponse.replace("<ADDR2>1222 Apt</ADDR2>", "<ADDR2></ADDR2>");
		webserviceResponse = webserviceResponse.replace("<CLNAM>SENTHIL CHINRAJ</CLNAM>", "<CLNAM></CLNAM>");
		
		MuleEvent event = runFlow("clientBrowseResponseDataMapperFlow",testEvent(webserviceResponse));
		
		String formattedXML = removeNamespace(event.getMessage());
		
		//Name Info Assert
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName", formattedXML);
		
		//Address Information Assert
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/City", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProv", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetNumber", formattedXML);
	}
	
	@Test
	public void testClientBrowseResponseDataMapperFlow_partial_information() throws MuleException, Exception{
		String webserviceResponse = clientBrowseWSResponse.replace("<ADDR3>TORONTO ON</ADDR3>", "<ADDR3>TORONTO</ADDR3>");
		webserviceResponse = webserviceResponse.replace("<ADDR1>24 Leith Hill Road</ADDR1>", "<ADDR1>24</ADDR1>");
		webserviceResponse = webserviceResponse.replace("<ADDR2>1222 Apt</ADDR2>", "<ADDR2>1222</ADDR2>");
		webserviceResponse = webserviceResponse.replace("<CLNAM>SENTHIL CHINRAJ</CLNAM>", "<CLNAM>SENTHIL</CLNAM>");
		
		MuleEvent event = runFlow("clientBrowseResponseDataMapperFlow",testEvent(webserviceResponse));
		
		String formattedXML = removeNamespace(event.getMessage());
		
		//Name Info Assert
		XMLAssert.assertXpathEvaluatesTo("SENTHIL", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName", formattedXML);
		
		//Address Information Assert
		XMLAssert.assertXpathEvaluatesTo("TORONTO", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/City", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProv", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("24", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("1222", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetNumber", formattedXML);
	}
	
	@Test
	public void testClientBrowseResponseDataMapperFlow_partial_information_withTrailingSpaces() throws MuleException, Exception{
		String webserviceResponse = clientBrowseWSResponse.replace("<ADDR3>TORONTO ON</ADDR3>", "<ADDR3>TORONTO  </ADDR3>");
		webserviceResponse = webserviceResponse.replace("<ADDR1>24 Leith Hill Road</ADDR1>", "<ADDR1>24  </ADDR1>");
		webserviceResponse = webserviceResponse.replace("<ADDR2>1222 Apt</ADDR2>", "<ADDR2>1222  </ADDR2>");
		webserviceResponse = webserviceResponse.replace("<CLNAM>SENTHIL CHINRAJ</CLNAM>", "<CLNAM>SENTHIL  </CLNAM>");
		
		MuleEvent event = runFlow("clientBrowseResponseDataMapperFlow",testEvent(webserviceResponse));
		
		String formattedXML = removeNamespace(event.getMessage());
		
		//Name Info Assert
		XMLAssert.assertXpathEvaluatesTo("SENTHIL", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/Surname", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/NameInfo/PersonName/GivenName", formattedXML);
		
		//Address Information Assert
		XMLAssert.assertXpathEvaluatesTo("TORONTO", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/City", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/StateProv", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("24", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[1]/StreetNumber", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetName", formattedXML);
		XMLAssert.assertXpathEvaluatesTo("1222", "/PolicyInqRs/PartyInqInfo/InsuredOrPrincipal/GeneralPartyInfo/Addr/DetailAddr[2]/StreetNumber", formattedXML);
	}
	
	@Test
	public void testclientWebServiceRepsonseHandlerFlow_happyPath() throws MuleException, Exception{
		XmlToXMLStreamReader reader = new XmlToXMLStreamReader();
		reader.initialise();
		Object obj=reader.transformMessage(testEvent(clientBrowseWSResponse).getMessage(), "UTF-8");
		MuleEvent message = runFlow("clientWebServiceRepsonseHandlerFlow", testEvent(obj));
		String output = removeNamespace(message.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//MsgStatus/MsgStatusCd",output);
	}
	
	@Test(expected=MessagingException.class)
	public void testclientWebServiceRepsonseHandlerFlow_soapInvalidResponse() throws MuleException, Exception{
		runFlow("clientWebServiceRepsonseHandlerFlow", testEvent("<GARBAGE></GARBAGE>"));
	}
	
}
