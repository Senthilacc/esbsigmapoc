package ca.rsagroup.diary.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class DiarySearchTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	
	 
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commondiary.xml,diarysearch.xml";
	}
	
	@Test
	public void testDiarySearchHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDiarySearchWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/DiarySearch_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("DiarySearch", testEvent(webserviceResponse));
		
		//Assert Webservice request
		
		XMLAssert.assertXpathEvaluatesTo("R735922","//SEARCH/SGMCTL_ENTITY/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20090201","//SEARCH/DIARYH_NATURAL_KEYS/ACTD1", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20141215","//SEARCH/DIARYH_NATURAL_KEYS/ACTD2", searchRq);
		XMLAssert.assertXpathEvaluatesTo("CBO","//SEARCH/DIARYH_NATURAL_KEYS/PERSN", searchRq);
		
		
		//Assert Response
		String diaryInqRs = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("8870727","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/PolicyNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("PME","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/LOBCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("2","//DiaryInqRs/DiaryItem[1]/SequenceNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("ABC","//DiaryInqRs/DiaryItem[1]/OwnerId", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("H","//DiaryInqRs/DiaryItem[1]/EntryType", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("N","//DiaryInqRs/DiaryItem[1]/FinalizationFlag", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("*************************ATTENTION ATTENTION******","//DiaryInqRs/DiaryItem[1]/ReferenceNumber", diaryInqRs);
		  
	} 
	
	
	@Test
	public void testDiarySearchWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDiarySearchWSResponseWithEDRMSG.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/DiarySearch_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("DiarySearch", testEvent(webserviceResponse));
		
		//Assert Webservice request
		
		XMLAssert.assertXpathEvaluatesTo("R735922","//SEARCH/SGMCTL_ENTITY/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20090201","//SEARCH/DIARYH_NATURAL_KEYS/ACTD1", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20141215","//SEARCH/DIARYH_NATURAL_KEYS/ACTD2", searchRq);
		XMLAssert.assertXpathEvaluatesTo("CBO","//SEARCH/DIARYH_NATURAL_KEYS/PERSN", searchRq);
		
		
		//Assert Response
		String diaryInqRs = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs);
	} 
	 
	
}
