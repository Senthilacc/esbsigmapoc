package ca.rsagroup.diary.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class DiaryDeleteTest extends BaseFunctionalMunitSuite { 
	 
	private String deleteRq;
	
	 
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml, diary.xml";
	}
	
	@Test
	public void testDiaryDeleteHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDeleteDiaryWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(deleteRq == null ) {
					deleteRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(deleteRq, event.getMuleContext());
					deleteRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/DeleteDiary_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("DeleteDiary", testEvent(webserviceResponse));
		
		//Assert Webservice request 
		 
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//GENERIC_DIARY_REQUEST/SGMCTL_ENTITY[1]/USRPF", deleteRq);
		
		XMLAssert.assertXpathEvaluatesTo("01","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/POLBR", deleteRq); 
		XMLAssert.assertXpathEvaluatesTo("AP","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/POLTY", deleteRq);
		XMLAssert.assertXpathEvaluatesTo("8139338","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/POLNO", deleteRq);
		XMLAssert.assertXpathEvaluatesTo("20090320","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/ACTDT", deleteRq);
		XMLAssert.assertXpathEvaluatesTo("2","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/SEQNO", deleteRq);
		XMLAssert.assertXpathEvaluatesTo("CBO","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/PERSN", deleteRq);
		
				
		//Assert Response
		
		String diaryInqRs = removeNamespace(event.getMessage()); 
				
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("8139338","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/PolicyNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/LOBCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("20090320","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo/ActivityDt", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("CBO","//DiaryInqRs/DiaryItem[1]/OwnerId", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("General description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", diaryInqRs);
			 
	}  
	
	@Test
	public void testEdrMessageEntityMapping() throws MuleException, Exception {
		
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDeleteDiaryWSResponseWithEDRMSG.xml")));

		String webserviceResponse = readFile("diary/DeleteDiary_CSIO_Request_DiaryInqRq.xml");

		MuleEvent event = runFlow("DeleteDiary", testEvent(webserviceResponse));
		
		//Assert Response
		
		String responsexml = removeNamespace(event.getMessage()); 
		
		//XMLAssert.assertXpathEvaluatesTo("updated description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("406","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20140615","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo[1]/ActivityDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Steve","//DiaryInqRs/DiaryItem[1]/OwnerId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Validation Failed","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/LOBCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("A","//DiaryInqRs/DiaryItem[1]/EntryType", responsexml);
		XMLAssert.assertXpathEvaluatesTo("8220473","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/PolicyNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("updated description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
	}
	 
}
