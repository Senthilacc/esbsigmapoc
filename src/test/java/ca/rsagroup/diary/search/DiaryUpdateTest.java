package ca.rsagroup.diary.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class DiaryUpdateTest extends BaseFunctionalMunitSuite { 
	 
	private String updateRq;
	
	 
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml, diary.xml";
	}
	
	@Test
	public void testDiaryCreateHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPUpdateDiaryWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(updateRq == null ) {
					updateRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(updateRq, event.getMuleContext());
					updateRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/UpdateDiary_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("UpdateDiary", testEvent(webserviceResponse));
		
		//Assert Webservice request 
		 
		XMLAssert.assertXpathEvaluatesTo("1","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/SEQNO", updateRq);
		XMLAssert.assertXpathEvaluatesTo("8870727","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/POLNO", updateRq);
		XMLAssert.assertXpathEvaluatesTo("20141215","//GENERIC_DIARY_REQUEST//DIARYH_ENTITY[1]/ACTDT", updateRq);
		XMLAssert.assertXpathEvaluatesTo("AP","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/POLTY", updateRq);
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//GENERIC_DIARY_REQUEST/SGMCTL_ENTITY[1]/USRPF", updateRq);
		XMLAssert.assertXpathEvaluatesTo("Updated description of diary","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/DES50", updateRq);
		XMLAssert.assertXpathEvaluatesTo("A","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/DIRYT", updateRq);
		XMLAssert.assertXpathEvaluatesTo("241545","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/AGTBR", updateRq);
		XMLAssert.assertXpathEvaluatesTo("Steve","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY[1]/PERSN", updateRq);
		
		//Assert Response
		
		String diaryInqRs = removeNamespace(event.getMessage()); 
		
		XMLAssert.assertXpathEvaluatesTo("updated description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("20140615","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo[1]/ActivityDt", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("Steve","//DiaryInqRs/DiaryItem[1]/OwnerId", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/LOBCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("A","//DiaryInqRs/DiaryItem[1]/EntryType", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("8220473","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/PolicyNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus[1]/MsgStatusCd", diaryInqRs);
		 
		 
	}  
	
	@Test
	public void testEdrMessageEntityMapping() throws MuleException, Exception {
		
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPUpdateDiaryWSResponseWithEDRMSG.xml")));

		String webserviceResponse = readFile("diary/CreateDiary_CSIO_Request_DiaryInqRq.xml");

		MuleEvent event = runFlow("UpdateDiary", testEvent(webserviceResponse));
		
		//Assert Response
		
		String responsexml = removeNamespace(event.getMessage()); 
		
		XMLAssert.assertXpathEvaluatesTo("updated description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("406","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20140615","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo[1]/ActivityDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Steve","//DiaryInqRs/DiaryItem[1]/OwnerId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Validation Failed","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/LOBCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("A","//DiaryInqRs/DiaryItem[1]/EntryType", responsexml);
		XMLAssert.assertXpathEvaluatesTo("8220473","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/PolicyNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus[1]/MsgStatusCd", responsexml);
	}
	 
}
