package ca.rsagroup.diary.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class DiaryCreateTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml, diary.xml";
	}
	
	@Test
	public void testDiaryCreateHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPCreateDiaryWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceRequest = readFile("diary/CreateDiary_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("CreateDiary", testEvent(webserviceRequest)); 
		
		//Assert Webservice request 
		 
		XMLAssert.assertXpathEvaluatesTo("UCQI0268","//GENERIC_DIARY_REQUEST/SGMCTL_ENTITY/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("241545","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/AGTBR", searchRq);
		XMLAssert.assertXpathEvaluatesTo("AP","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/POLTY", searchRq);
		XMLAssert.assertXpathEvaluatesTo("8870727","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/POLNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20141215","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/ACTDT", searchRq);
		XMLAssert.assertXpathEvaluatesTo("1","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/SEQNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("Steve","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/PERSN", searchRq);
		XMLAssert.assertXpathEvaluatesTo("H","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/DIRYT", searchRq);
		XMLAssert.assertXpathEvaluatesTo("General description of diary","//GENERIC_DIARY_REQUEST/DIARYH_ENTITY/DES50", searchRq);
		
		//Assert Response
		
		String diaryInqRs = removeNamespace(event.getMessage()); 
		
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("8870727","//DiaryInqRs/DiaryItem/PartyInqInfo/PartialPolicy/PolicyNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem/PartyInqInfo/PartialPolicy/LOBCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("241545","//DiaryInqRs/DiaryItem/AgencyBranchInfo/ContractNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("20141215","//DiaryInqRs/DiaryItem/NotifyMsgInfo/ActivityDt", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem/SequenceNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("Steve","//DiaryInqRs/DiaryItem/OwnerId", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("H","//DiaryInqRs/DiaryItem/EntryType", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("General description of diary","//DiaryInqRs/DiaryItem/GeneralDesc", diaryInqRs);
		 
	}  
	
	@Test
	public void testEdrMessageEntityMapping() throws MuleException, Exception {
		
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPCreateDiaryWSResponseWithEDRMSG.xml")));

		String webserviceResponse = readFile("diary/CreateDiary_CSIO_Request_DiaryInqRq.xml");

		MuleEvent event = runFlow("CreateDiary", testEvent(webserviceResponse));
		
		//Assert Response
		
		String responsexml = removeNamespace(event.getMessage()); 
		
		XMLAssert.assertXpathEvaluatesTo("General description of diary","//DiaryInqRs/DiaryItem[1]/GeneralDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("406","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("20141215","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo[1]/ActivityDt", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Steve","//DiaryInqRs/DiaryItem[1]/OwnerId", responsexml);
		XMLAssert.assertXpathEvaluatesTo("Validation Failed","//DiaryInqRs/MsgStatus[1]/ExtendedStatus[1]/ExtendedStatusDesc", responsexml);
		XMLAssert.assertXpathEvaluatesTo("AP","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/LOBCd", responsexml);
		XMLAssert.assertXpathEvaluatesTo("241545","//DiaryInqRs/DiaryItem[1]/AgencyBranchInfo[1]/ContractNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("H","//DiaryInqRs/DiaryItem[1]/EntryType", responsexml);
		XMLAssert.assertXpathEvaluatesTo("8870727","//DiaryInqRs/DiaryItem[1]/PartyInqInfo[1]/PartialPolicy[1]/PolicyNumber", responsexml);
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus[1]/MsgStatusCd", responsexml);

	}
	 
	
}
