package ca.rsagroup.diary.search;


import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.transformer.simple.ByteArrayToObject;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class DiaryBrowseTest extends BaseFunctionalMunitSuite { 
	 
	private String searchRq;
	
	 
	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,commondiary.xml,diarybrowse.xml";
	}
	
	@Test
	public void testDiaryBrowseHappyPath() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDiaryBrowseWSResponse.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/DiaryBrowse_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("DiaryBrowse", testEvent(webserviceResponse));
		
		//Assert Webservice request 
		
		XMLAssert.assertXpathEvaluatesTo("R735922","//BROWSE/SGMCTL_ENTITY/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("01","//BROWSE/DIARYH_PRIMARY_KEY/POLBR", searchRq); 
		XMLAssert.assertXpathEvaluatesTo("AP","//BROWSE/DIARYH_PRIMARY_KEY/POLTY", searchRq);
		XMLAssert.assertXpathEvaluatesTo("8139338","//BROWSE/DIARYH_PRIMARY_KEY/POLNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20090320","//BROWSE/DIARYH_PRIMARY_KEY/ACTDT", searchRq);
		XMLAssert.assertXpathEvaluatesTo("2","//BROWSE/DIARYH_PRIMARY_KEY/SEQNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("808180","//BROWSE/DIARYH_PRIMARY_KEY/CLMKY", searchRq);
		XMLAssert.assertXpathEvaluatesTo("CBO","//BROWSE/DIARYH_PRIMARY_KEY/PERSN", searchRq);
		
		//Assert Response
		
		String diaryInqRs = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("8220473","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/PolicyNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("MR","//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/LOBCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("20140615","//DiaryInqRs/DiaryItem[1]/NotifyMsgInfo/ActivityDt", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("1","//DiaryInqRs/DiaryItem[1]/SequenceNumber", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("ABC","//DiaryInqRs/DiaryItem[1]/OwnerId", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("N","//DiaryInqRs/DiaryItem[1]/ActionStatusCd", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("A","//DiaryInqRs/DiaryItem[1]/EntryType", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("N","//DiaryInqRs/DiaryItem[1]/FinalizationFlag", diaryInqRs);
		XMLAssert.assertXpathEvaluatesTo("test","//DiaryInqRs/DiaryItem[1]/GeneralDesc", diaryInqRs);
		 
	} 
	
	@Test
	public void testDiaryBrowseWithoutResult() throws MuleException, Exception{
		whenMessageProcessor("outbound-endpoint")
				.ofNamespace("http")
				.thenReturn(
						muleMessageWithPayload(readFile("diary/SOAPDiaryBrowseWSResponseWithEDRMSG.xml"))); 
		spyMessageProcessor("transform").ofNamespace("data-mapper").after(new SpyProcess() {
			
			@Override
			public void spy(MuleEvent event) throws MuleException {
				if(searchRq == null ) {
					searchRq = new ByteArrayToObject().doTransform(event.getMessage().getPayload(), "UTF-8").toString(); 
					MuleMessage newMessage = new DefaultMuleMessage(searchRq, event.getMuleContext());
					searchRq = removeNamespace(newMessage);
				}
			}
		});
	  
		String webserviceResponse = readFile("diary/DiaryBrowse_CSIO_Request_DiaryInqRq.xml");
		
		MuleEvent event = runFlow("DiaryBrowse", testEvent(webserviceResponse));
		
		//Assert Webservice request 
		
		XMLAssert.assertXpathEvaluatesTo("R735922","//BROWSE/SGMCTL_ENTITY/USRPF", searchRq);
		XMLAssert.assertXpathEvaluatesTo("01","//BROWSE/DIARYH_PRIMARY_KEY/POLBR", searchRq); 
		XMLAssert.assertXpathEvaluatesTo("AP","//BROWSE/DIARYH_PRIMARY_KEY/POLTY", searchRq);
		XMLAssert.assertXpathEvaluatesTo("8139338","//BROWSE/DIARYH_PRIMARY_KEY/POLNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("20090320","//BROWSE/DIARYH_PRIMARY_KEY/ACTDT", searchRq);
		XMLAssert.assertXpathEvaluatesTo("2","//BROWSE/DIARYH_PRIMARY_KEY/SEQNO", searchRq);
		XMLAssert.assertXpathEvaluatesTo("808180","//BROWSE/DIARYH_PRIMARY_KEY/CLMKY", searchRq);
		XMLAssert.assertXpathEvaluatesTo("CBO","//BROWSE/DIARYH_PRIMARY_KEY/PERSN", searchRq);
		
		//Assert Response
		
		String diaryInqRs = removeNamespace(event.getMessage());
		
		XMLAssert.assertXpathEvaluatesTo("OK","//DiaryInqRs/MsgStatus/MsgStatusCd", diaryInqRs); 
		XMLAssert.assertXpathNotExists("//DiaryInqRs/DiaryItem[1]/PartyInqInfo/PartialPolicy/PolicyNumber", diaryInqRs);
		 
	} 
	 
	 
	
}
