package ca.rsagroup.test;

import java.io.IOException;

import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.exceptions.XpathException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.api.transformer.TransformerException;
import org.mule.module.xml.transformer.XmlToXMLStreamReader;
import org.xml.sax.SAXException;

import ca.rsagroup.common.BaseFunctionalMunitSuite;

public class IntegrationTest extends BaseFunctionalMunitSuite {

	static XmlToXMLStreamReader reader;

	@BeforeClass
	public static void initialize() throws InitialisationException {
		reader = new XmlToXMLStreamReader();
		reader.initialise();
	} 

	@Override
	protected String getConfigResources() {
		return "commonFlows.xml,"
				+ "clientbrowse.xml,"
				+ "suspendedpolicysearch.xml,"
				+ "commondiary.xml," 
				+ "diarysearch.xml,diarybrowse.xml,"
				+ "commonbcalerts.xml,bcalertspostalcodebrowse.xml,bcalertspostalcodesearch.xml,bcalertsnamesearch.xml,bcalertsnamebrowse.xml,"
				+ "policysearch.xml";
	}

	@Override
	protected boolean haveToMockMuleConnectors() {
		return false;
	}

	@Test
	public void clientBrowse() throws Exception {
		runFlow("clientBrowseFlow",
				"client/ClientBrowse_CSIO_Request_PolicyInqRq.xml");
	}

	@Test
	public void suspendedPolicySearch() throws Exception {
		runFlow("SuspendedPolicySearch", "suspended/PolicyInqRq.xml");
	}

	@Test
	public void diarySearch() throws Exception {
		runFlow("DiarySearch", "diary/DiarySearch_CSIO_Request_DiaryInqRq.xml");
	}
	
	@Test
	public void diaryBrowse() throws Exception {
		runFlow("DiaryBrowse", "diary/DiaryBrowse_CSIO_Request_DiaryInqRq.xml");
	}
	
	@Test
	public void policySearch() throws Exception {
		runFlow("PolicySearchFlow", "policy/PolicySearch_CSIO_Request_PolicyInqRq.xml");
	}

	@Test
	public void BCNameBrowse() throws Exception {
		runFlow("BCNameBrowse", "bcalerts/BCAlertsNameBrowse_CSIO_Request_AccountInqRq.xml");
	}

	@Test
	public void BCNameSearch() throws Exception {
		runFlow("BCNameSearch", "bcalerts/BCAlertsNameSearch_CSIO_Request_AccountInqRq.xml");
	}
	
	@Test
	public void BCPostalCodeSearch() throws Exception {
		runFlow("BCPostalcodeSearch", "bcalerts/BCAlertsPostalCodeSearch_CSIO_Request_AccountInqRq.xml");
	}
	 
	@Test
	public void BCPostalCodeBrowse() throws Exception {
		runFlow("BCPostalcodeBrowse", "bcalerts/BCAlertsPostalCodeBrowse_CSIO_Request_AccountInqRq.xml");
	}


	private void runFlow(String flowName, String filePath)
			throws TransformerException, MuleException, IOException, Exception {
		MuleEvent event = runFlow(flowName, testEvent(parseFile(filePath)));
		assertIfMessageStatusIsOK(event);
	}

	private Object parseFile(String filePath) throws IOException,
			TransformerException, Exception {
		String csioRequest = readFile(filePath);
		Object obj = reader.transformMessage(testEvent(csioRequest)
				.getMessage(), "UTF-8");
		return obj;
	}

	private void assertIfMessageStatusIsOK(MuleEvent event)
			throws InitialisationException, TransformerException, SAXException,
			IOException, XpathException {
		String output = removeNamespace(event.getMessage());
		XMLAssert.assertXpathEvaluatesTo("OK", "//MsgStatus/MsgStatusCd",
				output);
	}

}
